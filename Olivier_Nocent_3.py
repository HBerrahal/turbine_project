import pandas as pd
import json
import folium
import Check_Gender as cd
import database as db


# Dessine une line polygonale à partir des coordonnées GPS contenues
# dans l'objet GeoJSON
def draw_polyline(geo_json, map, color="blue", weight=5, opacity=0.6):
  #  print(geo_json)
    data = json.loads(geo_json)

    if data['type'] == 'LineString':
        points = []
        for coord in data['coordinates']:
            points.append((coord[1], coord[0]))
        folium.PolyLine(points, color=color, weight=weight, opacity=opacity).add_to(map)

    if data['type'] == 'MultiLineString':
        for line in data['coordinates']:
            points = []
            for coord in line:
                points.append((coord[1], coord[0]))
            folium.PolyLine(points, color=color, weight=weight, opacity=opacity).add_to(map)


# Création d'une carte centrée sur Grenoble
fmap = folium.Map(location=[45.1875602, 5.7357819], tiles="OpenStreetMap", zoom_start=13)

# Initialisation du DataFrame df
# à partir des données du fichier CSV
df = pd.read_csv('/home/laurent/git/turbine_project/Donnees/Doc_Villes/PLANDEVILLE_VOIES_VDG.csv', sep=';')

# Dessin de la rue N°67 : Avenue Jean Perrot
print(df['GeoJSON'][67])
draw_polyline(df['GeoJSON'][67], fmap)
draw_polyline(df['GeoJSON'][77], fmap)
draw_polyline(df['GeoJSON'][405], fmap)
#draw_polyline(df['GeoJSON'][66], fmap)
#draw_polyline(df['GeoJSON'][74], fmap)
print(df['VOIE_COMPLET'][671])
#draw_polyline(df['GeoJSON'][671], fmap)
print(df['VOIE_COMPLET'][628])
#draw_polyline(df['GeoJSON'][628], fmap)

conn = db.create_connection()

for i in range(2,967):
    try:
       # print(i, df['VOIE_ID'][i], df['VOIE_COMPLET'][i], df['VOIE_FANTOIR'][i], df['VOIE_DATECRE'][i], df['VOIE_REAL'][i], df['VOIE_OFFICIEL'][i], df['TENANT'][i], df['ABOUTISSANT'][i], df['DENOM_ANNEE'][i], df['DM_SEANCE'][i], df['DELIB_NUM'][i], df['COTE_ARCHIVES'][i], df['DENOM_ORIGINE'][i], df['DENOM_N_MOINS_1'][i], df['DENOM_N_MOINS_1_ANNEE'][i], df['DENOM_N_MOINS_2'][i], df['DENOM_N_MOINS_2_ANNEE'][i], df['LIEN_EXTERNE'][i], df['OBSERVATIONS'][i], df['GeoJSON'][i].replace(';', ','))

      #  requete = """insert into nom_des_voies (voie_id, voie_complet, ville, code_postal, voie_fantoir, voie_date_cre, voie_real, voie_officiel, tenant, aboutissant, denom_annee, dm_seance, delib_num, cote_archives, denom_origine, lien_externe, observation, geojson , genre) Values("""

      #  requete = requete + db.quote(str(df['VOIE_ID'][i])) + ', ' + db.quote(str(df['VOIE_COMPLET'][i])) + ', ' + db.quote('Grenoble') + ', ' + db.quote('38000') + db.quote(str(df['VOIE_FANTOIR'][i])) + ', ' + db.quote(str(df['VOIE_DATECRE'][i])) + ', ' + db.quote(str(df['VOIE_REAL'][i])) + ', ' + db.quote(str(df['VOIE_OFFICIEL'][i])) + ', ' + db.quote(str(df['TENANT'][i])) + ', ' + db.quote(str(df['ABOUTISSANT'][i])) + ', ' + db.quote(str(df['DENOM_ANNEE'][i])) + ', ' + db.quote(str(df['DM_SEANCE'][i])) + ', ' + db.quote(str(df['DELIB_NUM'][i])) + ', ' + db.quote(str(df['COTE_ARCHIVES'][i])) + ', ' + db.quote(str(df['DENOM_ORIGINE'][i])) + ', ' + db.quote(str(df['DENOM_N_MOINS_1'][i])) + ', ' + db.quote(str(df['DENOM_N_MOINS_1_ANNEE'][i])) + ', ' + db.quote(str(df['LIEN_EXTERNE'][i])) + ', ' + db.quote(str(df['OBSERVATIONS'][i])) + ', ' + db.quote(str(df['GeoJSON'][i].replace(';', ',')))

        trouve, genre = cd.check_genre(df['VOIE_COMPLET'][i], debug=False)
 #       print(trouve, genre)

        if trouve:
            if genre=='masculin':
                draw_polyline(df['GeoJSON'][i].replace(';', ','), fmap)
            elif genre=='feminin':
                draw_polyline(df['GeoJSON'][i].replace(';', ','), color='red', map=fmap)
        else:
            genre = 'inconnu'
            draw_polyline(df['GeoJSON'][i].replace(';', ','), color='gray', map=fmap)

      #  requete = requete + ', ' + db.quote(genre) + ');'
      #  print(requete)
    except Exception as inst:
        print("Erreur", inst, i)

# Sauvegarde de la carte dans un fichier HTML
fmap.save("street.html")