import database


def import_bano():
    # On importe le fichier bano_ modifié, on génère les requêtes et on les sauvagarde dans le fichier final
    rue_en_cours = ''
    ancienne_rue = ''

    base_requete = "insert into nom_des_voies(voie_id, voie_complet, code_postal, ville, geojson) Values("
    compteur = 1
    str_geojson = ''

    fichier_final = open("Donnees/fichier_geojson_final.csv", "a")

    with open("Donnees/bano-38_modifié.csv", "r") as f:
        for line in f.readlines():
            # Traiter la ligne et ainsi de suite ...
            tab_split = line.strip().split(";")
            print(tab_split[0], tab_split[2], tab_split[5])

            # Maintenant on importe les données
            # Chaque rues de chaque ville ayant de nombreux point geojson, il faut lire toutes les lignes d'une rue et concaténer les différents points geojson

            rue_en_cours = tab_split[0]

            if rue_en_cours != ancienne_rue:
                # print(ancienne_rue, rue_en_cours, compteur)
                # on crée la requete d'insertion
                str_geojson = '{"type":"MultiLineString","coordinates":[' + str_geojson.strip('"\'') + ']'

                nom_rue = tab_split[0].replace("'", "''")
                nom_ville = tab_split[2].replace("'", "''")

#                print("GeoJson généré:" + str_geojson + "]}'")
                print("nom rue:", nom_rue)

                requete_valeur = str(compteur) + ", '" + nom_rue + "', '" + tab_split[1] + "', '" + nom_ville + "', '" + str_geojson[0:-2] + "]}');"
                str_geojson = tab_split[5] + ','

                requete_finale = base_requete + requete_valeur
                print(requete_finale)

                try:
                    fichier_final.write(requete_finale + "\n")
                    print('')

                except:
                    print(requete_finale)

               # print(requete_finale)
                requete_valeur = ''
                ancienne_rue = rue_en_cours
                compteur += 1

            else:
                str_geojson += tab_split[5] + ','

    fichier_final.close()
    f.close()


def import_plan_de_ville():

    fichier_final = open("Donnees/fichier_geojson_final.csv", "a")

    base_requete = "insert into nom_des_voies(voie_id, voie_complet, code_postal, ville, geojson) Values("

    with open("Donnees/PLANDEVILLE_VOIES_VDG_modifie.csv", encoding="ISO-8859-1") as f:
        compteur = 22870

        for line in f.readlines():
            # Traiter la ligne et ainsi de suite ...
            tab_split = line.strip().split("#")

            nom_rue = tab_split[1]
            nom_ville = tab_split[2]
            str_geojson = tab_split[3].strip('"\'')

            print("str_geojson: ", str_geojson)

            requete_valeur = str(compteur) + ", '" + nom_rue + "', '38000', '" + nom_ville + "', '" + str_geojson + "');"

            requete_finale = base_requete + requete_valeur
            print(requete_finale)
            compteur = compteur + 1

            try:
                fichier_final.write(requete_finale + "\n")
                #                cur = database.query_create_select(conn, requete_finale)

            except:
                print(requete_finale)

    fichier_final.close()
    f.close()

def import_fichier_requete():

    conn = database.create_connection()

    with open("Donnees/fichier_geojson_final.csv", encoding='utf-8') as f:
        for requete in f.readlines():
            print(requete)

            try:
                cur = database.query_create_select(conn, requete)

            except Exception as inst:
                print("Erreur", inst)
#:
                print(requete)


    f.close()

#import_bano()
#import_plan_de_ville()
import_fichier_requete()