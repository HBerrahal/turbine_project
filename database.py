import psycopg2
from psycopg2.extensions import ISOLATION_LEVEL_AUTOCOMMIT
from psycopg2.extras import  NamedTupleCursor
from os import path as os_path


def create_connection():
    """ Connect to the PostgreSQL database server """
    conn = None
    try:
        # connect to the PostgreSQL server
#        print('Connecting to the PostgreSQL database...')
        conn = psycopg2.connect(host="localhost",
                                dbname="carto_db",
                                user="carto",
                                password="carto_pwd",
                                port=5432)


        conn.set_isolation_level(ISOLATION_LEVEL_AUTOCOMMIT)

    except Exception as inst:
        print("I am unable to connect to the database")
        print(inst)

    return conn

def create_tables(conn) -> None:
    requete  ="DROP TABLE nom_des_voies;"

 #   query_create_select(conn, requete)

    requete = """CREATE TABLE IF NOT EXISTS nom_des_voies (
        voie_id serial, 
        voie_complet varchar(250), 
        ville varchar(40),
        code_postal varchar(5),
        voie_fantoir varchar(10), 
        voie_date_cre  varchar(10),
        voie_real varchar(15),
        voie_officiel varchar(15),
        tenant varchar(250),
        aboutissant varchar(250),
        denom_annee varchar(15),
        dm_seance varchar(10),
        delib_num varchar(15),
        cote_archives varchar(20),
        denom_origine varchar(350), 
        lien_externe varchar(350),
        observation varchar(300),
        geojson varchar(5000),
        genre varchar(10));"""

    query_create_select(conn, requete)

    requete = """CREATE TABLE IF NOT EXISTS coord_points_interets(
            idtf serial,
            titre varchar(100),
            thématiques varchar(100),
            periodes varchar(30),
            types varchar(100),
            adresse varchar(250),
            latitude float8,
            longitude float8,
            texte_fr text,
            Texte_en text,
            bibliographie varchar(250),
            site_internet varchar(250),
            credit varchar(250),
            legende varchar(250),
            genre varchar(10));"""

    # Mise à jour des genre des points d'intérêt
    # update    coord_points_interets    set    genre = 'masculin'    where    idtf in (325, 336, 340, 341, 348, 349, 350, 353, 358);
    # update    coord_points_interets    set    genre = 'feminin'    where    idtf in (327, 335, 344, 345, 346, 351, 356, 359);

    print('Création batiments_publics')

    requete = """CREATE TABLE IF NOT EXISTS batiments_publics(
                    equip_id serial,
                    equip_nom varchar(150),
                    adresse varchar(250),
                    coordonnees1 float8, 
                    coordonnees2 float8,
                    equip_web_lien varchar(250),
                    domaine_nom varchar(250),
                    adresse_id varchar(250),
                    telephone varchar(10),
                    courriel varchar(150),
                    horaire text,
                    description text,
                    info_supp text, 
                    genre  varchar(10))"""

    query_create_select(conn, requete)

    print('Création table Prénom')

    requete = """CREATE TABLE IF NOT EXISTS prenoms(
                    prenom varchar(20),
                    genre varchar(8),
                    langage varchar(200),
                    fréquence real)"""

    query_create_select(conn, requete)

    print('Création de la table des personnes célèbres')

    requete = """CREATE TABLE IF NOT EXISTS personnes_celebres(
                    prenom varchar(20),
                    nom varchar(30),
                    genre varchar(8))"""

    query_create_select(conn, requete)

def query_create_select(conn, requete):
    cur = conn.cursor()
    cur.execute(requete)
    return cur


def query_dict(conn, requete, data_dict):
    cur = conn.cursor(cursor_factory=psycopg2.extras.RealDictCursor)
    cur.execute(requete, data_dict)
    return cur

def import_ville(ville, code_postal):
    # on importe en base de données les rues d'une ville depuis le fichier "ville".txt (ville est le nom passé en paramêtre ainsi que son code code postal
    PATH = os_path.abspath(os_path.split(__file__)[0])

    print(PATH + "/Donnees/Doc_Villes/" + ville + ".txt")

    with open(PATH + "/Donnees/Doc_Villes/" + ville + ".txt", "r") as f:
        fichier_entier = f.read()
        lignes = fichier_entier.split("\n")

        for ligne in lignes:
            print(ligne)

def import_prenom(conn, fichier_prenom):
    with open(fichier_prenom) as f:
        next(f)

        fichier_entier = f.read()
        lignes = fichier_entier.split("\n")

        requete = "Insert into prenoms(prenom, genre, langage, fréquence) Values("
        requete_valeur = ""
        requete_finale = ""

        for ligne in lignes:
            valeur = ligne.split(";")
            if valeur[1] == "f":
                genre = "feminin"
            elif valeur[1] == "m":
                genre = "masculin"
            elif valeur[1] == "m":
                genre = "msc/fem"
            requete_valeur = quote(valeur[0]) + ", " + quote(genre) + ", " + quote(valeur[2]) + ", " + valeur[3] + ");"
            requete_finale = requete + requete_valeur
            print(requete_finale)
            query_create_select(conn, requete_finale)


def maj_genre_suivant_prenom(conn):

    requete  = "Select voie_id, voie_complet from nom_des_voies;"
    cur = query_create_select(conn, requete)

    for ligne in cur:
        try:
            print(ligne[0])
            print(ligne[1])
            voie_id = ligne[0]
            tab_split = ligne[1].split(' ')
            trouve = False
            genre = 'neutre'

            for mot in tab_split:
                mot = mot.replace("'", "''").lower()
                requete_mot = "Select * From prenoms where lower(prenom) = '" + mot + "' and langage like '%french%';"
                cur2 = query_create_select(conn, requete_mot)

                if cur2:
                    for ligne2 in cur2:
                        if ligne2[1] is not None:
                            print("Genre: ", ligne2[1], mot)
                            requete2 = "Update nom_des_voies set genre = " + quote(ligne2[1]) + " Where voie_id = " + str(voie_id) + ";"
                            print(requete2)
                            cur = query_create_select(conn, requete2)

        except Exception as inst:
            print("Erreur", inst)


def ajout_deux_points(conn):

    requete = "Select voie_id, geojson From  nom_des_voies Where geojson like '{\"type\":\"LineString\",\"coordinates\"[[%' order by ville;"
    cur = query_create_select(conn, requete)

    for ligne in cur:
        try:
            print(ligne[0], ligne[1])
            tab_split = ligne[1].split('[[')
            requete_update = "update nom_des_voies set geojson = " + quote(str(tab_split[0]) + ':[[' + tab_split[1]) + " Where voie_id = " + str(ligne[0]) + ";"
            print(requete_update)
            cur2 = query_create_select(conn, requete_update)

        except Exception as inst:
            print("Erreur", inst)

def export_vers_csv():
    requete = "Select * From nom_des_voies Where geojson like '{\"type\":\"LineString\",\"coordinates\":[[%' order by ville, voie_id;"
    print(requete)
    cur = query_create_select(conn, requete)

    mon_fichier = open("/home/laurent/git/turbine_project/Donnees/Doc_Villes/PLANDEVILLE_VOIES_VDG_perso.csv", "w")  # Argh j'ai tout écrasé !

    for ligne in cur:
        try:
            ligne_a_ecrire = ''
            for i in range(0,18):
                if i != 18:
                    ligne_a_ecrire = ligne_a_ecrire + str(ligne[i]) + ';'
                else:
                    ligne_a_ecrire = ligne_a_ecrire + '\"' + str(ligne[i]) + '\";'

            mon_fichier.write(ligne_a_ecrire + ';\n')

            print(ligne_a_ecrire)
        except Exception as inst:
            print("Erreur", inst)

    mon_fichier.close()


def quote(chaine):
    return "'" + chaine + "'"


if __name__ == "__main__":
    with create_connection() as conn:
        try:
            #create_tables(conn)
            #maj_genre_suivant_prenom(conn)
            #ajout_deux_points(conn)
            #import_prenom(conn, "/home/laurent/git/turbine_project/Donnees/Prenoms.csv")
            export_vers_csv()
        except Exception as e:
            print(e)
        finally:
            conn.commit()