import urllib.request
import json
import csv
import os
import database
from geopy.geocoders import Nominatim

# file path setting
OPEN_STREETS_NAMES_FILE = 'all-streets.txt'
INPUT_DIR = 'Donnees/Doc_Villes/'
OUTPUT_DIR = 'output'
OUTPUT_CSV_FILENAME = 'street-coordinates.csv'
OUTPUT_JSON_FILENAME = 'geojson.json'

# nominatim api address
BASE_URL = 'https://nominatim.openstreetmap.org/?format=json&polygon_geojson=1&addressdetails=1&q='

# init
CSV_DATA = [['id', 'name', 'coordinates']]

GEOJSON = {
    "type": "FeatureCollection",
    "features": []
}

COUNTRY_CODE = 'fr'


def flatten(something):
    if isinstance(something, (list, tuple, set, range)):
        for sub in something:
            yield from flatten(sub)
    else:
        yield something


def split(arr, count):
    return [arr[i:i + count] for i in range(0, len(arr), count)]

def get_coordinates(ville, code_postal, index_debut):
    ID = 0

    print(INPUT_DIR + ville + '.txt')

    conn = database.create_connection()

    with open(INPUT_DIR + ville + '.txt') as f:
        lines = f.read().splitlines()
        for street in lines:
            search_street_name = street.strip().replace(" ", "+")
            url = f'{BASE_URL}{search_street_name}+{ville}'
            print(url)

            try:
                res_body = urllib.request.urlopen(url).read()
                print('après urllib.request')
                content = json.loads(res_body.decode("utf-8"))
                streets = [x for x in content if x['address']['country_code'] == COUNTRY_CODE]
                streets = [d for d in streets if d.get('geojson', -1) != -1]
                coordinates = [street['geojson']['coordinates'] for street in streets if
                               street['geojson']['type'] == 'LineString']

                all_coordinates = []
                row = []

                if len(streets) > 0:
                    all_coordinates = split(list(flatten(coordinates)), 2)

                ID = ID + 1
                row = [ID, street, all_coordinates]

                # add to csv
                CSV_DATA.append(row)

                street_geojson = {
                    "id": ID,
                    "type": "Feature",
                    "geometry": {
                        "type": "LineString",
                        "coordinates": all_coordinates},
                    "properties": {
                        "name": street
                    }
                }

                # add to geojson
                GEOJSON['features'].append(street_geojson)

                street_str = str(street.replace("'", "''"))
                str_coord = '{"type":"LineString","coordinates":' + str(all_coordinates)
                print(str_coord)
#                newurl = html_unescape(newurl.encode('ascii', 'ignore'))

                requete = 'insert into nom_des_voies(voie_id, voie_complet, ville, code_postal, geojson) values('
                requete += str(index_debut) + ", '" + street_str + "', '" + ville + "', '" + code_postal + "', '" + str(str_coord) + "}');"
                index_debut += 1

                print(requete)

        #        database.query_create_select(conn, requete)

            except Exception as inst:
                print("Erreur", inst, street)
#                print("Erreur", inst, street, requete)


    if not os.path.exists(OUTPUT_DIR):
        os.makedirs(OUTPUT_DIR)

    with open(f'{OUTPUT_DIR}/{OUTPUT_CSV_FILENAME}', 'w') as csvFile:
        writer = csv.writer(csvFile)
        writer.writerows(CSV_DATA)
        print("writing completed")
        csvFile.close()

    with open(f'{OUTPUT_DIR}/{OUTPUT_JSON_FILENAME}', 'w') as f:
        f.write(json.dumps(GEOJSON, sort_keys=True,
                           indent=4, separators=(',', ': ')))

    return GEOJSON

def get_coordinates2(adresse, code_postal, ville):

    geolocator = Nominatim()
    try:
        location = geolocator.geocode(adresse + " " + code_postal + " " + ville)
        print(location)
        return location.raw
    except:
        return None

def get_all_goejson():

    conn = database.create_connection()
    cur = database.query_create_select(conn, "select distinct voie_id, voie_complet, code_postal, ville, genre from nom_des_voies where ville = 'Grenoble' order by voie_id;")

    requete_insert = "Update nom_des_voies set geojson = '"

    for ligne in cur:
        print(ligne[0], ligne[1], ligne[2])
        dict_coord = get_coordinates2(ligne[1], ligne[2], ligne[3])
        print(dict_coord)


        if dict_coord is not None:
            geojson_final = '{"type":"MultiLineString","coordinates":[[' + dict_coord['boundingbox'][0] + ", " + dict_coord['boundingbox'][2] + "], [" + dict_coord['boundingbox'][1] + ", " + dict_coord['boundingbox'][3] + "]]}"
            print("geojson: ", geojson_final)

            requete_finale = requete_insert + str(geojson_final) + "' Where voie_id = " + str(ligne[0]) + ";"

            print(requete_finale)
    #        cur2 = database.query_create_select(conn, requete_finale)

def essai_une_coord():
    conn = database.create_connection()

    search_street_name = 'avenue+jean+perrot'
    search_street_name = 'rue+paul+heroult'
    search_street_name = 'Avenue+Marie+Reynoard'
    street = search_street_name
    ville = 'La+Tronche'
    ville = 'Grenoble'
    url = f'{BASE_URL}{search_street_name}+{ville}'
    print(url)

    try:
        res_body = urllib.request.urlopen(url).read()
        print('après urllib.request')
        content = json.loads(res_body.decode("utf-8"))
        streets = [x for x in content if x['address']['country_code'] == COUNTRY_CODE]
        streets = [d for d in streets if d.get('geojson', -1) != -1]
        coordinates = [street['geojson']['coordinates'] for street in streets if
                       street['geojson']['type'] == 'LineString']

        all_coordinates = []
        row = []

        if len(streets) > 0:
            all_coordinates = split(list(flatten(coordinates)), 2)

        print("all_coordinates: ", coordinates)
        all_coordinates_inverse = []
        for mots in all_coordinates:
            coord_final = []

            for mot in reversed(mots):
                coord_final.append(mot)

            all_coordinates_inverse.append(coord_final)
        print("all_coordinates inversée: ", all_coordinates_inverse)


        street_geojson = {
            "id": 1,
            "type": "Feature",
            "geometry": {
                "type": "LineString",
                "coordinates": all_coordinates},
            "properties": {
                "name": street
            }
        }

        # add to geojson
        GEOJSON['features'].append(street_geojson)

        street_str = str(street.replace("'", "''"))
        str_coord = '{"type":"LineString","coordinates":' + str(all_coordinates)
        print(str_coord)

    except Exception as inst:
        print("Erreur", inst)


#print(get_coordinates("Echirolles", "38130", 3000))
#get_coordinates("Echirolles", "38130", 3000)
#get_coordinates("Meylan", "38240", 5000)
#get_coordinates("Fontaine", "38600", 4000)
#get_coordinates("Eybens", "38320", 7000)
#get_coordinates("Seyssinet-Pariset", "38170", 8000)
#get_coordinates("La Tronche", "38700", 9000)
#print(get_coordinates2("rue Pierre Ruibet", "38000", "Grenoble"))
#get_all_goejson()
essai_une_coord()
