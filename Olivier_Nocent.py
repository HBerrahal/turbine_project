import pandas as pd
import json
import folium
import scrap_google as sc
import database
from flask import Flask


debug = 0
# Dessine une line polygonale à partir des coordonnées GPS contenues
# dans l'objet GeoJSON
def draw_polyline(geo_json, map, tooltip, color="blue", weight=5, opacity=0.6):
    print("geo_json: {}".format(geo_json[1:len(geo_json)-1]))
    data = json.loads(geo_json)

    if data['type'] == 'LineString':
        points = []
        for coord in data['coordinates']:
            points.append(coord)
        folium.PolyLine(points, color=color, weight=weight, opacity=opacity).add_to(map)

    if data['type'] == 'MultiLineString':
        for line in data['coordinates']:
            points = []
            for coord in line:
                print("coord2: ", coord)
                points.append(coord)
            folium.PolyLine(points, color=color, weight=weight, opacity=opacity).add_to(map)

def check_gender(nom_rue):
    debug = 1
    stopwords = ['la', 'le', 'des', 'de', 'Père', 'point', 'Saint', 'Place',
                 'Rue', 'Avenue', 'Allée', 'Quai', 'Rond', 'Chemin', 'Passage', 'Cours',
                 'Boulevard', 'Impasse', 'Général', 'Lieutenant', 'Route', 'Cour', 'Galerie',
                 'Président', 'Prosper', 'ème', 'Régiment', 'Jardin', 'Champ', 'La', 'Le',
                 'et', 'Lys', 'Docteur', 'ter', 'Capitaine', 'Parc', 'Square', 'Stade', 'bis',
                 'Voie', 'Pont', 'Commandant', 'Sainte', 'Colonel', 'Espace']


    prenom = nom_rue.split(" ")[1]
    if prenom in stopwords:
        prenom = nom_rue.split(" ")[2]
        if prenom in stopwords:
            prenom = nom_rue.split(" ")[3]

    nom = nom_rue.split(" ")[2]
    if debug == 1:
        if debug: print(prenom, nom)
    d = sc.check_genre(prenom)
    if debug == 1:
        if debug: print(f'prenom :{prenom} genre:{d}')

    return d

app = Flask(__name__)

# Création d'une carte centrée sur Grenoble
fmap = folium.Map(location=[45.17, 5.7357819], tiles="OpenStreetMap", zoom_start=14)

lgd_txt = '<span style="color: {col};">{txt}</span>'
group_routes_masculines = folium.FeatureGroup(name=lgd_txt.format(txt="Routes Masculines", col="blue"))
group_routes_feminines = folium.FeatureGroup(name=lgd_txt.format(txt="Routes Féminines", col="red"))
group_routes_neutres = folium.FeatureGroup(name=lgd_txt.format(txt="Routes Neutres", col="green"))
group_ecoles_feminines = folium.FeatureGroup(name=lgd_txt.format(txt="Ecoles (Fem.)", col="purple"))
group_ecoles_masculines = folium.FeatureGroup(name=lgd_txt.format(txt="Ecoles (Masc.)", col="blue"))
group_ecoles_neutres = folium.FeatureGroup(name=lgd_txt.format(txt="Ecoles Neutres", col="gray"))
group_routes_masculines.add_to(fmap)
group_routes_feminines.add_to(fmap)
group_routes_neutres.add_to(fmap)
group_ecoles_feminines.add_to(fmap)
group_ecoles_masculines.add_to(fmap)
group_ecoles_neutres.add_to(fmap)

@app.route('/')
def index():
    conn = database.create_connection()

#    cur = database.query_create_select(conn, "select distinct voie_id, voie_complet, genre, geojson from nom_des_voies Where ville = 'Echirolles';")
    requete = "select distinct voie_id, voie_complet, genre, geojson from nom_des_voies Where ville in ('Grenoble', 'Meylan', 'Chamrousse') And geojson <> '{""""type"""":""""LineString"""",""""coordinates"""":[]}';"
    #requete = "select distinct voie_id, voie_complet, genre, geojson from nom_des_voies Where voie_id = 22937";
    if debug:
        print("requete: ", requete)

    print("requete: ", requete)
    cur = database.query_create_select(conn, requete)

    if cur is not None:
        for ligne in cur:
            print(ligne[0], ligne[3])
            genre = ligne[2]
            if debug: print("geojson: ", ligne[3])

            if debug: print("genre: ", genre)
            genre = "masculin"

            try:
                if genre == "masculin":
                    draw_polyline(ligne[3], group_routes_masculines, tooltip=ligne[1])
                elif genre == "feminin":
                    draw_polyline(ligne[3], group_routes_feminines, color="red", tooltip=ligne[1])
                else:
                    draw_polyline(ligne[3], group_routes_neutres, color="green", tooltip=ligne[1])

            except  Exception as inst:
                print("Erreur", inst, ligne[3])


    Ligne_a_tracer = '{"type":"MultiLineString","coordinates":[[[5.73364716776952,45.1833490915976],[5.73365648420878,45.183104221506],[5.73380659712687,45.1829987912521],[5.73408063895113,45.1824687529321],[5.73431546049113,45.1820145641972],[5.73442103662995,45.1818002210245],[5.73460874096931,45.181419135052],[5.73464278272215,45.181350021144],[5.73469257004274,45.1812489391841],[5.73473335527602,45.1811661363776],[5.73484149572557,45.180946586752],[5.73500946430215,45.1806055689409],[5.73537359936673,45.1798662557243],[5.73551561641535,45.1795779033254],[5.73564441465872,45.1793164042969],[5.73573109461773,45.1791404098186],[5.73574055323101,45.1791212050366],[5.7360006506231,45.17859309759],[5.73656982236416,45.1774374029174],[5.73668803768429,45.1771973623058],[5.7370390102978,45.1764846927512],[5.73733088076805,45.1758920051698],[5.73761562399733,45.1753138047437],[5.73860450919768,45.1733056125307],[5.7386876015978,45.1731368666387],[5.73901076394597,45.1724805740298],[5.73904206181909,45.1724170120996],[5.73940227411225,45.1716854479694],[5.73954822951268,45.1713890188278],[5.73990506332672,45.1706642910497],[5.74008603004498,45.170296735017],[5.74067136048772,45.1691078675425],[5.74089712665689,45.1686492886598],[5.74146125270934,45.1675034331676]],[[5.74161987381538,45.1671812294014],[5.74188132238335,45.1666489304056]]]}'
    print(Ligne_a_tracer)
    draw_polyline(Ligne_a_tracer, fmap, tooltip="Test")


    # Maintenant on affiche les points d'intérêt
    cur = database.query_create_select(conn, "SELECT equip_nom, coordonnees2, coordonnees1, genre From batiments_publics where equip_nom like 'Ecole%' order by equip_nom;")
    cur = None

    if cur is not None:
        for ligne in cur:
            try:
                if debug: print(ligne[0], ligne[2])
                genre = ligne[3]
                if debug: print(genre)

                nom_ecole = ligne[0]

                nom_ecole = nom_ecole.replace("é", "&eacute;")
                nom_ecole = nom_ecole.replace("è", "&egrave;")
                nom_ecole = nom_ecole.replace("ë", "&euml;")
                nom_ecole = nom_ecole.replace("ï", "&icirc;")
                nom_ecole = nom_ecole.replace("ï", "&iuml;")
                nom_ecole = nom_ecole.replace("à", "&agrave;")
                nom_ecole = nom_ecole.replace("ù", "&ugrave;")
                nom_ecole = nom_ecole.replace("ç", "&ccedil;")

                if genre == "masculin":
                    folium.Marker([ligne[1], ligne[2]], nom_ecole, icon=folium.Icon(color='blue', icon='glyphicon-user')).add_to(group_ecoles_masculines)
                elif genre == "feminin":
                    folium.Marker([ligne[1], ligne[2]], nom_ecole, icon=folium.Icon(color='red', icon='glyphicon-user')).add_to(group_ecoles_feminines)
                else:
                    folium.Marker([ligne[1], ligne[2]], nom_ecole, icon=folium.Icon(color='gray', icon='glyphicon-user')).add_to(group_ecoles_neutres)

            except Exception as inst:
                if debug: print("Erreur", inst)

    rue_jean_perrot = '{"type": "MultiLineString", "coordinates": [[[5.73364716776952, 45.1833490915976], [5.73365648420878, 45.183104221506], [5.73380659712687, 45.1829987912521], [5.73408063895113, 45.1824687529321], [5.73431546049113, 45.1820145641972], [5.73442103662995, 45.1818002210245], [5.73460874096931, 45.181419135052], [5.73464278272215, 45.181350021144], [5.73469257004274, 45.1812489391841], [5.73473335527602, 45.1811661363776], [5.73484149572557, 45.180946586752], [5.73500946430215, 45.1806055689409], [5.73537359936673, 45.1798662557243], [5.73551561641535, 45.1795779033254], [5.73564441465872, 45.1793164042969], [5.73573109461773, 45.1791404098186], [5.73574055323101, 45.1791212050366], [5.7360006506231, 45.17859309759], [5.73656982236416, 45.1774374029174], [5.73668803768429, 45.1771973623058], [5.7370390102978, 45.1764846927512], [5.73733088076805, 45.1758920051698], [5.73761562399733, 45.1753138047437], [5.73860450919768, 45.1733056125307], [5.7386876015978, 45.1731368666387], [5.73901076394597, 45.1724805740298], [5.73904206181909, 45.1724170120996], [5.73940227411225, 45.1716854479694], [5.73954822951268, 45.1713890188278], [5.73990506332672, 45.1706642910497], [5.74008603004498, 45.170296735017], [5.74067136048772, 45.1691078675425], [5.74089712665689, 45.1686492886598], [5.74146125270934, 45.1675034331676]], [[5.74161987381538, 45.1671812294014], [5.74188132238335, 45.1666489304056]]]'
    try:
        draw_polyline(rue_jean_perrot, group_routes_masculines, "rue Jean Perr")
    except  Exception as inst:
        print("Erreur", inst)

    # Sauvegarde de la carte dans un fichier HTML
    fmap.save("templates/Olivier.html")
    folium.LayerControl().add_to(fmap)
    return fmap._repr_html_()

if __name__ == '__main__':
    app.run(port=5001)
#    app.run(debug=True)