import database
import json

def export_db():
    # on ouvre le fichier texte d'export
    fichier = open("export_donnees/nom_des_voies.txt", "w")

    # On ouvre la connexion à la base de données et on lance la requete sur la table nom_des_voies
    conn = database.create_connection()

#    cur = database.query_create_select(conn, "select * from nom_des_voies where geojson not like '%[[[%' order by voie_id;")
    cur = database.query_create_select(conn, "Select distinct * From nom_des_voies order by voie_id;")

    requete_insert = "insert into nom_des_voies(voie_id, voie_complet, voie_fantoir, voie_date_cre, voie_real, voie_officiel, tenant, aboutissant, denom_annee, dm_seance, delib_num, cote_archives, denom_origine, lien_externe, observation, geojson, genre) values("

    ligne = 0
    for row in cur:
        requete_valeurs = ""

        for i in range(1, 19):
            requete_valeurs = requete_valeurs + "'" + str(row[i]).replace("'", "''") + "', "

        requete_finale = requete_insert + str(row[0]) + ", " + requete_valeurs[0:len(requete_valeurs) - 2] + ");"
        print(requete_finale)

        fichier.write("%s\n" % (requete_finale))

    fichier.close()

    # on ouvre le fichier texte d'export
    fichier = open("export_donnees/points_interet.txt", "w")

    # On charge les points d'intérêts depuis la base de données
    cur = database.query_create_select(conn, "select * from coord_points_interets order by idtf;")

    requete_insert = "insert into coord_points_interets(idtf, titre, thématiques, periodes, types, adresse, latitude, longitude, texte_fr, texte_en, bibliographie, site_internet, credit, legende) values("

    ligne = 0
    for row in cur:
        requete_valeurs = ""

        for i in range(1, 14):
            requete_valeurs = requete_valeurs + "'" + str(row[i]).replace("'", "''") + "', "

        requete_finale = requete_insert + str(row[0]) + ", " + requete_valeurs[0:len(requete_valeurs) - 2] + ");"
 #       print(requete_finale)

        fichier.write("%s\n" % (requete_finale))

    fichier.close()

    # on ouvre le fichier texte d'export
    fichier = open("export_donnees/batiments_publics.txt", "w")

    # On charge les ecoles depuis la base de données
    cur = database.query_create_select(conn, "Select distinct * From batiments_publics order by domaine_nom;")

    requete_insert = "insert into batiments_publics(equip_id, equip_nom, adresse, coordonnees1, coordonnees2, equip_web_lien , domaine_nom, adresse_id, telephone, courriel, horaire, description, info_supp, genre) values("

    for row in cur:
        requete_valeurs = ""

        for i in range(1, 14):
            print(i, str(row[i]))
            requete_valeurs = requete_valeurs + "'" + str(row[i]).replace("'", "''") + "', "

        requete_finale = requete_insert + str(row[0]) + ", " + requete_valeurs[0:len(requete_valeurs) - 2] + ");"
        print(requete_finale)

        fichier.write("%s\n" % (requete_finale))

    fichier.close()




def import_db():

    import_nom_des_voies()
    import_points_interet()
    import_public()

def import_nom_des_voies():
    # on ouvre le fichier texte d'export
    fichier = open("export_donnees/nom_des_voies_save.txt", "r")
    # On ouvre la connexion à la base de données et on lance la requete sur la table nom_des_voies
    conn = database.create_connection()

    for ligne in fichier:
        print(ligne)
        cur = database.query_create_select(conn, ligne)

    fichier.close()

def import_points_interet():
    # on ouvre le fichier texte d'export
    fichier = open("export_donnees/points_interet.txt", "r")

    # On ouvre la connexion à la base de données et on lance la requete sur la table nom_des_voies
    conn = database.create_connection()

    for ligne in fichier:
        cur = database.query_create_select(conn, ligne)

    fichier.close()

def import_public():
    # on ouvre le fichier texte d'export
    fichier = open("export_donnees/batiments_publics.txt", "r")

    # On ouvre la connexion à la base de données et on lance la requete sur la table nom_des_voies
    conn = database.create_connection()

    for ligne in fichier:
        print(ligne)
        cur = database.query_create_select(conn, ligne)

    fichier.close()


def import_json_equipements_grenoble():


    # prompt the user for a file to import
    filter = "JSON file (*.json)|*.json|All Files (*.*)|*.*||"
    filename = "/home/laurent/git/turbine_project/Donnees/Equipement_ESPG4326.json"

    data = []
    dict_valeur = {}
    dict_properties = {}
    with open(filename) as f:
        # Reading from file
        data = json.loads(f.read())

        # on ouvre la connexion à la base de données
        conn = database.create_connection()

        coordonnees1 = 0.0
        coordonnees2 = 0.0
        equip_id = 0
        equip_nom = ""
        adresse = ""
        equip_web_lien = ""
        domaine_nom = ""
        adresse_id = ""
        telephone = ""
        courriel = ""
        horaire = ""
        description = ""
        info_supp = ""

        for key, value in data.items():
            if key == "features":
                for key2 in value:
                    coordonnees1 = key2['geometry']['coordinates'][0]
                    coordonnees2 = key2['geometry']['coordinates'][1]

                    dict_properties = key2
                    for key3 in key2:
                        print("key3: ", key3)
                        if key3 == "coordinates":
                            for key4 in key3:
                                print("key4: ", key4)
                    try:
                        equip_id = dict_properties['properties']['EQUIP_ID']
                        equip_nom = dict_properties['properties']['EQUIP_NOM'].replace("'", "''")
                        adresse = dict_properties['properties']['ADRESSE'].replace("'", "''")
                        equip_web_lien = dict_properties['properties']['EQUIP_WEB_LIEN']
                        domaine_nom = dict_properties['properties']['DOMAINE_NOM'].replace("'", "''")
                        adresse_id = dict_properties['properties']['ADRESSE_ID']
                        telephone = dict_properties['properties']['TELEPHONE'][0:7].replace(" ", "")
                        courriel = dict_properties['properties']['COURRIEL']
                        horaire = dict_properties['properties']['HORAIRE'].replace("'", "''")
                        description = dict_properties['properties']['DESCRIPTION'].replace("'", "''")
                        info_supp = dict_properties['properties']['INFO_SUPP'].replace("'", "''")
                    except Exception  as inst:
                        print("Erreur")

                    requete = "Insert into batiments_publics(equip_id, equip_nom, adresse, coordonnees1, coordonnees2, equip_web_lien, domaine_nom, adresse_id, telephone, courriel, horaire, description, info_supp) values("
                    requete = requete + str(equip_id) + ", '" + equip_nom + "', '" + adresse + "', '" + str(coordonnees1) + "', '" + str(coordonnees2) + "', '" + equip_web_lien + "', '" + domaine_nom + "', '" + adresse_id + "', '" + telephone + "', '" + courriel + "', '" + horaire + "', '" + description + "', '" + info_supp + "');"

                    print(requete)



                    cur = database.query_create_select(conn, requete)

        # "EQUIP_ID","EQUIP_NOM","ADRESSE","EQUIP_WEB_LIEN","DOMAINE_NOM","ADRESSE_ID","TELEPHONE","COURRIEL","HORAIRE","DESCRIPTION","INFO_SUPP"


            # Closing file
        f.close()

    # with open(filename, 'r') as f:
    #     mon_json = json.load(f)
    #     print(mon_json)
    #     print(mon_json['name'])
    #     print(mon_json['features'])
    #     print(mon_json['features'][0]['type'])
    #     print(mon_json['features'][0]['properties']['EQUIP_NOM'])
    #     print("Coordonnées: ", mon_json['features'][0]['geometry']['coordinates'])

export_db()
#import_db()
#import_public()
#import_points_interet()
#import_json_equipements_grenoble()