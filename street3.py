from flask import Flask
import folium
import database
import pandas as pd
import json

app = Flask(__name__)


@app.route('/')
def index():
    # on charge la base de données centrée sur Grenoble
    m = folium.Map(location=[45.1875602, 5.7357819], tiles="OpenStreetMap", zoom_start=13.5)
    folium.raster_layers.TileLayer('Open Street Map').add_to(m)
    folium.raster_layers.TileLayer('Stamen Terrain').add_to(m)
    folium.raster_layers.TileLayer('Stamen Toner').add_to(m)
    folium.raster_layers.TileLayer('Stamen Watercolor').add_to(m)
    folium.raster_layers.TileLayer('CartoDB Positron').add_to(m)
    folium.raster_layers.TileLayer('CartoDB Dark_Matter').add_to(m)

    # On crée le groupe "Points d'Intérêt"
    lgd_txt = '<span style="color: {col};">{txt}</span>'

    group01_fem = folium.FeatureGroup(name=lgd_txt.format(txt="Art et Culture (fem.)", col="darkgreen"))
    group01_masc = folium.FeatureGroup(name=lgd_txt.format(txt="Art et Culture (masc.)", col="green"))
    group02_fem = folium.FeatureGroup(name=lgd_txt.format(txt="Sciences et Techniques (fem.)", col="gray"))
    group02_masc = folium.FeatureGroup(name=lgd_txt.format(txt="Sciences et Techniques (masc.)", col="lightgray"))
    group03_fem = folium.FeatureGroup(name=lgd_txt.format(txt="Patrimoine Naturel(fem.)", col="red"))
    group03_masc = folium.FeatureGroup(name=lgd_txt.format(txt="Patrimoine Naturel (masc.)", col="darkred"))
    group04_fem = folium.FeatureGroup(name=lgd_txt.format(txt="Histoire et Evolution (fem.)", col="darkblue"))
    group04_masc = folium.FeatureGroup(name=lgd_txt.format(txt="Histoire et Evolution (masc.)", col="blue"))

    # On ajoute les groupes à la carte
    group01_fem.add_to(m)
    group01_masc.add_to(m)
    group02_fem.add_to(m)
    group02_masc.add_to(m)
    group03_fem.add_to(m)
    group03_masc.add_to(m)
    group04_fem.add_to(m)
    group04_masc.add_to(m)

    # group2 = folium.FeatureGroup(name='<span style=\\"color: blue;\\">Routes Masculines</span>')
    group2 = folium.FeatureGroup(name=lgd_txt.format(txt="Routes Masculines", col="blue"))

    geojson = {"type": "MultiLineString", "coordinates": [
        [[45.1877083140852, 5.72903804011517], [45.1880655502734, .73002109318556],
         [45.1885126965622, 5.73125816953391]],
        [[45.1854939439964, 5.7229452369528], [45.1858877081056, 5.72402857532704],
         [45.1861234464337, 5.72467716737003], [45.1871522984843, 5.72750804934443],
         [45.1875535393851, 5.72861213915014]]]}
    geojson = {"type": "MultiLineString", "coordinates": [[[5.72903804011517, 45.1877083140852], [5.73002109318556, 45.1880655502734], [5.73125816953391, 45.1885126965622]], [[5.7229452369528, 45.1854939439964], [5.72402857532704, 45.1858877081056], [5.72467716737003, 45.1861234464337], [5.72750804934443, 45.1871522984843], [5.72861213915014, 45.1875535393851]]]}
    geojson = {"type": "LineString", "coordinates": [[5.71648353516516, 45.1803585484208], [5.71778461011853, 45.1804518997561], [5.71861505883699, 45.1804988391244], [5.71960392247255, 45.1805599100649], [5.71981966548885, 45.1805737323225], [5.72036793907523, 45.1806132800465], [5.72099025874189, 45.1806544448308], [5.72136782060671, 45.1806807353921], [5.7214990420279, 45.1806901289583], [5.7216236699311, 451807006507078], [5.72169222128087, 45.1807082920384], [5.72175339037734, 45.1807151034901], [5.72178995704826, 45.1807191276292], [5.72260461418428, 45.1808184038412], [5.72315782700051, 45.180912478521], [5.72490573160229, 45.1812457553313], [5.72496710555305, 45.1812593490072], [5.72527092062365, 45.1813268925749], [5.7253170634448, 45.1813371564629], [5.72535390537879, 45.1813453498551], [5.72546615780698, 45.1813707171565], [5.72635771487256, 45.1816204441743], [5.72706771460924, 45.1818952029586]]}


    #    geojson = "{\"type\":\"MultiLineString\",\"coordinates\":[[[5.72903804011517,45.1877083140852],[.73002109318556,45.1880655502734],[5.73125816953391,45.1885126965622]],[[5.7229452369528,45.1854939439964],[5.72402857532704,45.1858877081056],[5.72467716737003,45.1861234464337],[5.72750804934443,45.1871522984843],[5.72861213915014,45.1875535393851]]]}"
    #    geojson = "{\"type\":\"MultiLineString\",\"coordinates\":[[5.72996019471978,45.1935892543962],[5.72992606191922,45.1934612897873],[5.72979651059553,45.1929755937918]]}"
    #    geojson = "{\"type\":\"LineString\",\"coordinates\":[[5.73410995731548,45.1968156184737],[5.73489472622548,45.1958272250026]]}"
    tooltiptext = "rue Jean Perrot"

    compteur = 0

    couleur = "blue"  # On affiche les rues dont le nom est masculin

    try:
        print("geojson: ", type(geojson["coordinates"]), geojson["coordinates"])

        geo_list = list(geojson["coordinates"])
        taille_liste_geo = len(geo_list)

        i = 1

        if taille_liste_geo > 1:
            for coordonnees in geo_list:
                for coordonnee in coordonnees:
                    print("coordonnee{}: ".format(i), coordonnee)
                folium.PolyLine(coordonnees, color=couleur, weight=5, opacity=0.6, popup=tooltiptext).add_to(group2)
                i = i + 1

        else:
            for g_li in geo_list:
                print("g_li: ", g_li)
        #        data = json.loads(geojson)

        #    for datum in data["coordinates"]:
        #        print("datum: ", datum)

        #           folium.PolyLine(points, color=couleur, weight=5, opacity=0.6, popup=tooltiptext).add_to(group2)

        print("Après folium.PolyLine")

    except Exception as inst:
        print("Erreur:", inst)

    group2.add_to(m)
    folium.LayerControl().add_to(m)
    m.save("templates/street3.html")
    return m._repr_html_()


if __name__ == '__main__':
    app.run(port=5000)
#    app.run(debug=True)
